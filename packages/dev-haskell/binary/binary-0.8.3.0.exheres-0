# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Binary serialisation for Haskell values using lazy ByteStrings"
DESCRIPTION="
Efficient, pure binary serialisation using lazy ByteStrings. Haskell values may be encoded to and
from binary formats, written to disk as binary, or sent over the network. Serialisation speeds of
over 1 G/sec have been observed, so this library should be suitable for high performance scenarios.
"
HOMEPAGE="http://code.haskell.org/binary/ ${HOMEPAGE}"

LICENCES="BSD-3"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""
# break annoying dependency cicle
RESTRICT="test"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/array
        dev-haskell/bytestring[>=0.10.2]
        dev-haskell/containers
    ")
    $(haskell_test_dependencies "
        dev-haskell/Cabal
        dev-haskell/HUnit
        dev-haskell/QuickCheck[>=2.8]
        dev-haskell/array
        dev-haskell/bytestring[>=0.10.2]
        dev-haskell/containers
        dev-haskell/directory
        dev-haskell/filepath
        dev-haskell/random[>=1.0.1.0]
        dev-haskell/test-framework
        dev-haskell/test-framework-quickcheck2[>=0.3]
    ")
"

