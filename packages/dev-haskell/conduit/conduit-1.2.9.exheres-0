# Copyright 2012 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Streaming data processing library."
DESCRIPTION="
conduit is a solution to the streaming data problem, allowing for production,
transformation, and consumption of streams of data in constant memory. It is an
alternative to lazy I/O which guarantees deterministic resource handling.

For more information about conduit in general, and how this package in
particular fits into the ecosystem, see the conduit
homepage.

Hackage documentation generation is not reliable. For up to date documentation, please see: http://www.stackage.org/package/conduit.
"
HOMEPAGE="http://github.com/snoyberg/conduit"

LICENCES="MIT"
PLATFORMS="~amd64"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/exceptions[>=0.6]
        dev-haskell/lifted-base[>=0.1]
        dev-haskell/mmorph
        dev-haskell/monad-control
        dev-haskell/mtl
        dev-haskell/resourcet[=1.1*]
        dev-haskell/transformers[>=0.2.2]
        dev-haskell/transformers-base[~>0.4.1]
    ")
    $(haskell_test_dependencies "
        dev-haskell/QuickCheck[>=2.7]
        dev-haskell/containers
        dev-haskell/exceptions[>=0.6]
        dev-haskell/hspec[>=1.3]
        dev-haskell/mtl
        dev-haskell/resourcet
        dev-haskell/safe
        dev-haskell/split[>=0.2.0.0]
        dev-haskell/transformers
    ")
"

