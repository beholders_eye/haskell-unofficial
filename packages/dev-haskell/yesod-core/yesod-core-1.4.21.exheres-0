# Copyright 2011, 2012 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Creation of type-safe, RESTful web applications"

LICENCES="MIT"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/aeson[>=0.5]
        dev-haskell/auto-update
        dev-haskell/blaze-builder[>=0.2.1.4&<0.5]
        dev-haskell/blaze-html[>=0.5]
        dev-haskell/blaze-markup[>=0.5.1]
        dev-haskell/byteable
        dev-haskell/bytestring[>=0.10]
        dev-haskell/case-insensitive[>=0.2]
        dev-haskell/cereal[>=0.3]
        dev-haskell/clientsession[>=0.9.1&<0.10]
        dev-haskell/conduit[>=1.2]
        dev-haskell/conduit-extra
        dev-haskell/containers[>=0.2]
        dev-haskell/cookie[>=0.4.1&<0.5]
        dev-haskell/data-default
        dev-haskell/deepseq
        dev-haskell/directory[>=1]
        dev-haskell/exceptions[>=0.6]
        dev-haskell/fast-logger[>=2.2]
        dev-haskell/http-types[>=0.7]
        dev-haskell/lifted-base[>=0.1.2]
        dev-haskell/monad-control[>=0.3&<1.1]
        dev-haskell/monad-logger[>=0.3.1&<0.4]
        dev-haskell/mtl
        dev-haskell/mwc-random
        dev-haskell/old-locale[>=1.0.0.2&<1.1]
        dev-haskell/parsec[>=2&<3.2]
        dev-haskell/path-pieces[>=0.1.2&<0.3]
        dev-haskell/primitive
        dev-haskell/random[>=1.0.0.2&<1.2]
        dev-haskell/resourcet[>=0.4.9&<1.2]
        dev-haskell/safe
        dev-haskell/semigroups
        dev-haskell/shakespeare[>=2.0]
        dev-haskell/template-haskell
        dev-haskell/text[>=0.7]
        dev-haskell/time[>=1.1.4]
        dev-haskell/transformers[>=0.2.2]
        dev-haskell/transformers-base[>=0.4]
        dev-haskell/unix-compat
        dev-haskell/unordered-containers[>=0.2]
        dev-haskell/vector[>=0.9&<0.12]
        dev-haskell/wai[>=3.0]
        dev-haskell/wai-extra[>=3.0.7]
        dev-haskell/wai-logger[>=0.2]
        dev-haskell/warp[>=3.0.2]
        dev-haskell/word8
    ")
    $(haskell_test_dependencies "
        dev-haskell/HUnit
        dev-haskell/QuickCheck[>=2&<3]
        dev-haskell/async
        dev-haskell/blaze-builder
        dev-haskell/bytestring
        dev-haskell/clientsession
        dev-haskell/conduit
        dev-haskell/conduit-extra
        dev-haskell/containers
        dev-haskell/cookie[>=0.4.1&<0.5]
        dev-haskell/hspec[>=1.3]
        dev-haskell/hspec-expectations
        dev-haskell/http-types
        dev-haskell/lifted-base
        dev-haskell/mwc-random
        dev-haskell/network
        dev-haskell/path-pieces
        dev-haskell/random
        dev-haskell/resourcet
        dev-haskell/shakespeare
        dev-haskell/streaming-commons
        dev-haskell/template-haskell
        dev-haskell/text
        dev-haskell/transformers
        dev-haskell/wai[>=3.0]
        dev-haskell/wai-extra
    ")
"

# test want to listen on public interface
RESTRICT="test"

